export interface FormConfig {
    data?: any; // Define initial data
    schema?: any; // Define the schema for the data
    uischema?: any; // Define the UI schema for interactive forms
    debug?: boolean; // Show additional debug information on the forms
}
