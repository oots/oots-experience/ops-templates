// Copyright (C) 2024 European Union
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
//
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import { apiFetch } from "../gitlab/api";
import { AuthConfig } from "./config";
import { AuthProvider, AuthProviders } from "./core";
import { OAuth2LoginProvider, OAuthConfig } from "./providers/oauth2";
import { TokenAuthProvider, TokenConfig } from "./providers/tokens";
import { AuthSession } from "./session";

export const RegisteredAuthProviders: AuthProviders = {
  oauth2: (config: OAuthConfig) => new OAuth2LoginProvider(config),
  tokens: (config: TokenConfig) => new TokenAuthProvider(config),
};

export class AuthContext {
  type: string;

  // Resolve current provider by type and or name
  get Providers(): AuthProviders {
    return RegisteredAuthProviders;
  }
  get Provider(): AuthProvider {
    return this.getProvider(this.type);
  }

  // Accessors for current auth session details
  get Session(): AuthSession | null {
    return AuthSession.Current;
  }
  set Session(session: AuthSession | null) {
    AuthSession.Current = session;
  }

  constructor(private options: AuthConfig) {
    // Add authenication provider details
    this.type = options?.type;

    // Detect expired tokens from previous sessions
    const session = this.Session;
    if (session) this.restoreSession(session);
  }

  isAuthenticated() {
    return !!this.Session?.ready;
  }

  newSession(type: string, state: any): AuthSession {
    return new AuthSession(type, state);
  }

  getProvider(type: string): AuthProvider {
    return this.Providers[type]?.call(this, this.options[type]);
  }

  async restoreSession(session: AuthSession) {
    const provider = this.getProvider(session.type);
    if (provider && session?.ready) {
      // Detect and auto refresh the session if needed
      this.Session = (await provider.detect()) || session;
    }
  }

  async getSession(): Promise<AuthSession | null> {
    // Check for any persisted sessions
    let session = this.Session;
    if (session?.ready) return session;
    if (session) {
      this.type = session.type;
      // Check for updates and callbacks to resolve
      session = (await this.Provider?.detect()) || session;
    }
    return session;
  }

  async createSession(): Promise<AuthSession | null> {
    if (!this.type) this.type = this.options?.type;

    // Create a new login sesssion
    const provider = this.Provider;
    if (provider) {
      const config = this.options ? this.options[this.type] : {};
      return await provider.create(config);
    }

    // No login provider found...
    return null;
  }

  async startLogin(session: AuthSession) {
    // Persist the current auth state, so wecan retrieve it later
    this.Session = session;

    // Use the provider to perform the login
    const provider = this.getProvider(session.type);
    if (provider) {
      session = await provider.login(session);
      this.type = session?.type;
      this.Session = session;
    } else throw new Error(`AuthProvider '${this.type}' not found.`);
  }

  async signOut(session: AuthSession) {
    // Forget the current login session and force a reload of the page
    const provider = this.getProvider(session.type);
    if (provider && session) {
      this.Session = null;
      await provider.logout(session);
    }
    this.Session = null;
  }

  async getProfile() {
    const session = this.Session;
    if (!session) return null;
    if (!session.ready) return null;
    if (!session.profile) {
      // Fetch user profile and store it in the cache
      const { name, username, avatar_url, email, web_url } = await apiFetch("/api/v4/user/", {});
      session.profile = { name, username, avatar_url, email, web_url };
      this.Session = session;
    }
    return session.profile;
  }
}
