// Copyright (C) 2024 European Union
// 
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// 
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

export const STORE_NAME = 'auth.session';

export class AuthSession {
    constructor(
        public type: string,
        public state: any = {},
        public ready: boolean = false,
        public profile?: any,
        public trigger?: any,
    ) { }

    // Track current auth session globally in storage
    static get Current(): AuthSession | null {
        const cached = JSON.parse(sessionStorage.getItem(STORE_NAME) || 'null');
        if (cached) return new AuthSession(
            cached.type,
            cached.state,
            cached.ready,
            cached?.profile,
            cached?.trigger
        );
        return null;
    }
    static set Current(value: AuthSession | null) {
        if (value) {
            sessionStorage.setItem(STORE_NAME, JSON.stringify(value));
        } else {
            sessionStorage.removeItem(STORE_NAME);
        }
    }
}