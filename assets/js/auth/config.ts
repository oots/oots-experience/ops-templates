export interface AuthConfig {
    type: string;
    
    [provider: string]: any & { enabled: boolean };
}