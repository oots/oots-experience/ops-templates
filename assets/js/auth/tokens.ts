// Copyright (C) 2024 European Union
// 
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// 
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import { AppContext } from "../app";
import { apiFetch } from "../gitlab/api";
import { AuthContext } from "./context";
import { AuthSession } from "./session";

export interface TriggerToken {
    id: number;
    token?: string;
    owner?: {
        name: string;
        username: string;
    };
}

export class AuthTokens {
    protected Triggers: Record<string, TriggerToken[]>;

    get Trigger(): TriggerToken {
        const session = AuthSession.Current;
        return session?.trigger;
    }
    set Trigger(trigger: TriggerToken | null) {
        let session = AuthSession.Current;
        if (session) session.trigger = trigger
        AuthSession.Current = session;
    }

    constructor(private project_id: string) { }

    async get(): Promise<TriggerToken | null> {
        const session = AuthSession.Current;
        const { project_id } = this;
        let trigger = this.Trigger;
        if (trigger) return trigger;

        // Fetch user profile if not already available
        if (session && !session.profile) {
            session.profile = await AppContext.Current.Auth?.getProfile()
        }

        // No cached token, fetch list of tokens (if exists) or create a new token
        let triggers = this.Triggers && this.Triggers[project_id];
        if (!triggers) {
            triggers = await this.fetch();
        }

        if (triggers?.length > 0) {
            trigger = triggers[0]; // Default to the first available token            
            this.Trigger = trigger
        }

        // Generate a new token if none exists
        if (!trigger && !triggers?.length) {
            let label = `Token for ${session?.profile?.name}`;
            trigger = this.Trigger = await this.create(label);
            triggers = this.Triggers[project_id] = [trigger];
        }

        this.Triggers = this.Triggers || { };
        this.Triggers[project_id] = triggers;

        return trigger;
    }

    async set(token: TriggerToken) {
        this.Trigger = token;
    }

    async fetch() {
        let session = AuthSession.Current;
        if (!session) return null;

        // Filter the list of tokens to only include tokens for the current user
        const triggers = await apiFetch(`/api/v4/projects/${this.project_id}/triggers`);
        return triggers?.filter(t => t.owner?.username == session?.profile?.username).map(({id, token, description}) => ({id, token, description}) );
    }

    async create(desc: string): Promise<TriggerToken> {
        const session = AuthSession.Current;

        if (!desc) throw new Error('Description is required to create trigger token.');
        if (!session) throw new Error('No authentication session could be found.');

        return await apiFetch(`/api/v4/projects/${this.project_id}/triggers`, {
            method: "POST",
            headers: { "content-type": "application/json" },
            body: { description: desc },
        }).catch(ex => {
            // Create of token failed
            console.error(ex.message, ex);

            // Display message to user explaining he does not have the correct permissions
            const { id_token } = session.state;
            const jwt_body = id_token?.indexOf('.') >= 0 ? id_token.split('.')[1] : null;
            const info = jwt_body ? JSON.parse(atob(jwt_body)) : {};
            const groups = info?.groups_direct || [];
            alert(`Token creation failed. 
            
You might not have 'maintainer' permissions on this project.

You have the following permissions:
 - ${groups.join('\n - ')}

Please contact your system administrator to add the permissions.
`);
        });
    }

    async delete(token: TriggerToken) {
        const { project_id } = this;
        if (!token) return false
        try {
            // Send request to delete
            const req_url = `/api/v4/projects/${project_id}/triggers/${token.id}`;
            await apiFetch(req_url, { method: "DELETE" });

            // Refresh list of cached tokens
            this.Triggers = this.Triggers || {};
            this.Triggers[project_id] = await this.fetch();
            if (this.Trigger?.id == token.id) this.Trigger = null;
            return true;
        } catch (ex) {
            const err_msg = `Failed to delete the trigger token with id: ${project_id}`;
            console.error(err_msg);
            return false
        }
    }

}