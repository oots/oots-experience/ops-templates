// Copyright (C) 2024 European Union
// 
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// 
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import { apiFetch } from '../../gitlab/api';
import { AuthProvider } from '../core'
import { AuthSession } from '../session'


export interface TokenConfig {
  enabled: boolean
  allowed: string[]
}

export class TokenAuthProvider implements AuthProvider {

  constructor(private config: TokenConfig) { }

  async detect(): Promise<AuthSession | null> {
    if (!this.config) return null;
    if (this.config?.enabled === false) return null;

    // Look for existing session in user storage
    let session = AuthSession.Current;
    if (!session) return await this.findLegacyToken();
    if (session?.type !== 'tokens') return null;
    console.debug(`auth:${session?.type}`, 'Found session', session);

    return session;
  }

  async create(config: any): Promise<AuthSession> {
    const session = new AuthSession('tokens', {
      type: 'PRIVATE-TOKEN',
      token: this.findLegacyToken(),
    });
    console.debug(`auth:${session?.type}`, 'create()', { config, session });
    return session;
  }

  async login(session: AuthSession): Promise<AuthSession> {
    const { type, token } = session?.state || {}
    if (!token) throw new Error(`No token was specified.`);

    // Try and fetch the user profile with the specified token
    console.debug(`auth:${session?.type}`, 'login()', { type });
    const user = await apiFetch('/api/v4/user/', {
      headers: this.headers(session),
      session: null,
    });

    // Mark current session as ready and persist
    session.ready = true;
    AuthSession.Current = session;

    // Reload the page
    window.location.reload();
    return new Promise<AuthSession>(() => console.log('Refreshing page...'));
  }

  async logout(session: AuthSession): Promise<AuthSession> {
    session.ready = false;
    window.location.reload();
    return new Promise<AuthSession>(() => console.log('Signing out...'));
  }

  headers(session: AuthSession): Record<string, string> {
    const headers = {}
    const { type, token } = session?.state || {}
    if (token && type) {
      headers[type] = token;
    }
    return headers;
  }

  private async findLegacyToken() {
    const type = 'PRIVATE-TOKEN';
    const token = localStorage.getItem('workflow.token');
    if (token) {
      console.debug(`auth:token`, 'Found legacy token', { type });
      return new AuthSession('tokens', { type, token });
    }
    return null;
  }
}
