// Copyright (C) 2024 European Union
// 
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// 
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import { AppContext } from '../../app'
import { AppConfig } from '../../config'
import { AuthProvider } from '../core'
import { AuthSession } from '../session'


export interface OAuthConfig {
  enabled: boolean
  client_id: string
  scope: string
  auth_url: string
  token_url: string
  callback_url: string
  return_url: string
}

export class OAuth2LoginProvider implements AuthProvider {

  constructor(private config: OAuthConfig) { }

  async detect(): Promise<AuthSession | null> {
    if (!this.config) return null;
    if (this.config?.enabled === false) return null;

    // Look for existing session in user storage
    let session = AuthSession.Current;
    if (session?.type !== 'oauth2') return null;
    console.debug(`auth:${session?.type}`, `Found session`, session);

    // Check for callback from auth server to convert into auth tokens
    const params = new URLSearchParams(window.location.search);
    const code = params.get("code");
    const state = params.get("state");
    if (code && state && session?.state?.state == state) {
      console.debug(`auth:${session?.type}`, 'Check returned code and state', { code, state });
      session = await this.fetch_auth_tokens(session, code);
    }

    // Check if the token has expired and set up schedule for refresh token
    const timeout = this.getTimeToLive(session);
    const { refresh_token, created_at, expires_in } = session?.state || {};
    if (timeout <= 0) {
      // The token has expired, try and use the refresh token to get a new one
      console.debug(`auth:${session?.type}`, 'Token expired, refresh', { refresh_token, timeout, created_at, expires_in });
      session = await this.refresh_auth_tokens(session);
    } else {
      // The token will expire in the future, so we schedule a refresh just before that
      setTimeout(() => session && this.refresh_auth_tokens(session), timeout);
    }

    return session;
  }

  async create(config: any): Promise<AuthSession> {
    const state = crypto.randomUUID();
    const code_verifier = this.random_str(128);
    const code_challenge = await this.code_challenge(code_verifier);
    const session = new AuthSession('oauth2', {
      state,
      code_verifier,
      code_challenge,
      login_url: await this.get_login_url(this.config, state, code_challenge),
      return_url: this.config?.return_url || this.get_return_url(),
    });
    console.debug(`auth:${session?.type}`, 'create()', { config, session });
    return session;
  }

  async login(session: AuthSession): Promise<AuthSession> {
    const { login_url } = session?.state || {}
    if (login_url) {
      // Redirect for auth to identity provider
      window.location.href = login_url;
    }
    return new Promise<AuthSession>(() => console.log('Redirecting for authentication...', login_url));
  }

  async logout(session: AuthSession): Promise<AuthSession> {
    session.ready = false;    
    window.location.reload();
    return new Promise<AuthSession>(() => console.log('Signing out...'));
  }

  headers(session: AuthSession): Record<string, string> {
    if (!session.ready) return {}
    const { token_type, access_token } = session.state;
    return { Authorization: `${token_type} ${access_token}` }
  }

  private async get_login_url(config: OAuthConfig, state: string, code_challenge: string) {
    const { auth_url, client_id, callback_url: redirect_uri, scope } = config;
    const params = {
      response_type: "code",
      redirect_uri,
      client_id,
      scope,
      state,
      code_challenge,
      code_challenge_method: "S256",
    };
    const params_str = Object.keys(params)
      .map((key) => `${key}=${encodeURIComponent(params[key])}`)
      .join("&");

    // --> Example requests: https://example.com/oauth/authorize?response_type=code&...&code_challenge_method=S256
    // <-- Will redirect to: https://example.com/oauth/redirect?code=1234567890&state=STATE
    return `${auth_url}?${params_str}`;
  }

  private get_return_url(): string {
    return (
      new URLSearchParams(window.location.search).get("ret") ||
      window.location.href
    );
  }

  private random_str(length: number): string {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~";
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
  }

  private getTimeToLive(session: AuthSession): number {
    const { created_at, expires_in } = session?.state || {};
    if (created_at && expires_in) {
      const preflight = 1; // expires_in - 10
      const expires = (created_at + expires_in - preflight) * 1000;
      const timeout = expires - Date.now();
      return timeout;
    }
    return -1;
  }

  private async code_challenge(input: string): Promise<string> {
    const buffer = new TextEncoder().encode(input);
    const hash = await crypto.subtle.digest("SHA-256", buffer);
    const base64 = btoa(String.fromCharCode(...new Uint8Array(hash)));
    return base64.replace(/\+/g, "-").replace(/\//g, "_").replace(/\=+$/, "");
  }

  private async fetch_auth_tokens(session: AuthSession, code: string): Promise<AuthSession> {
    const { api } = AppContext.Current?.Config || {};
    const { state: { code_verifier, return_url } } = session;
    const { token_url, client_id, callback_url } = this.config;
    const params = {
      client_id,
      redirect_uri: callback_url,
      grant_type: "authorization_code",
      code_verifier,
      code,
    };
    const request = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(params),
    }

    // Sanity check for SSRF 
    //if (token_url.indexOf(api) !== 0) throw new Error('token_url is not a valid API request');

    console.debug(`auth:${session?.type}`, 'Get auth tokens', { token_url, params });
    const data = await fetch(token_url, request)
      .then((resp) => resp.json())
      .catch((ex) => {
        console.error("Request failed", ex.message);
        return null;
      });

    console.debug(`auth:${session?.type}`, 'Auth response', { token_url, data });

    if (data) {
      // Update the current session state
      session.state = { code_verifier, ...data }
      session.ready = true;
      AuthSession.Current = session;
    }

    // Redirect to original page
    if (return_url) window.location.href = return_url;

    return session;
  }

  private async refresh_auth_tokens(session: AuthSession): Promise<AuthSession> {
    const { api } = AppContext.Current?.Config || {};
    const { state: { code_verifier, refresh_token, scope } } = session;
    const { token_url, client_id, callback_url } = this.config;
    const params = {
      grant_type: "refresh_token",
      client_id,
      redirect_uri: callback_url,
      code_verifier,
      refresh_token,
    };
    const request = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(params),
    }
    
    // Sanity check for SSRF 
    //if (token_url.indexOf(api) !== 0) throw new Error('token_url is not a valid API request');
    console.debug(`auth:${session?.type}`, 'Refresh expired token', { token_url, session, current: AuthSession.Current });
    const data = await fetch(token_url, request)
      .then((resp) => resp.json())
      .catch((ex) => {
        console.error("Request failed", ex.message);
        return null;
      });

    console.debug(`auth:${session?.type}`, 'Tokens were refreshed', { refresh_token: data.refresh_token, data });
    if (data) {
      // Update the current session state
      session.state = { code_verifier, ...data }
      AuthSession.Current = session;
    }

    // The token will expire in the future, so we schedule a refresh just before that
    const timeout = this.getTimeToLive(session);
    if (timeout > 0) {
      setTimeout(() => session && this.refresh_auth_tokens(session), timeout);
    }

    return session;
  }
}
