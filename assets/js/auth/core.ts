// Copyright (C) 2024 European Union
// 
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// 
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

import { AuthSession } from './session'

export class AuthProviders {
    [key: string]: (config: any) => AuthProvider;
};

export interface AuthProvider {
    detect(): Promise<AuthSession | null>;
    create(config: any): Promise<AuthSession>;
    login(session: AuthSession): Promise<AuthSession>;
    logout(session: AuthSession): Promise<AuthSession>;
    headers(session: AuthSession): Record<string, string>;
}
