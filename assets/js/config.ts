import { AuthConfig } from "./auth/config";
import { WorkflowConfig } from "./workflows/config";

// SOme global helper functions for retrieving state
export const getStore = (key: string) => localStorage.getItem(key);
export const setStore = (key: string, value: string) => localStorage.setItem(key, value);
export const getBranch = () => {
  return new URLSearchParams(window.location.search).get("branch");
}

export class AppConfig {
  base: string;
  api: string;
  auth?: AuthConfig;
  workflows?: WorkflowConfig;

  constructor({ base, api, auth, workflows }: any = {}) { 
    this.base = base;
    this.api = api;
    this.auth = auth;
    this.workflows = workflows;
  }
}
