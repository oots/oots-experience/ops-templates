import { AuthTokens } from "../auth/tokens";
import { getBranch } from "../config";
import { apiFetch } from "../gitlab/api";
import { isPending } from "./config";

export interface PipelineInfo {
    id: number;
    project_id: string;
    ref: string;
    status: string;
    source: string;
    created_at: string;
}

export class PipelineContext {
    constructor(protected project_id: string) { }

    async fetch(pipeline_id: number) {
        const { project_id } = this;
        return await apiFetch(`/api/v4/projects/${project_id}/pipelines/${pipeline_id}`);
    }

    async start(variables = {}) {
        const { project_id } = this;
        const ref = getBranch();
        const action = `/api/v4/projects/${project_id}/trigger/pipeline`;
        const trigger = await new AuthTokens(project_id).get();
        const token = trigger?.token;
        return await apiFetch(action, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: {
                ref,
                token,
                variables
            }
        });
    }

    async waitFor(pipeline: PipelineInfo) {
        const id = pipeline.id;
        console.debug("Wait for pipeline", [id], pipeline);

        // Wait for the pipeline to complete
        while (isPending(pipeline.status)) {
            await new Promise((res) => setTimeout(res, 1000)); // Wait a second
            pipeline = await this.fetch(id);
        }

        // Handle error conditions for wwhen a pipeline does not complete
        if (["error", "canceled"].indexOf(pipeline.status) >= 0) {
            const error = `Pipeline '${id}' failed, status: ${pipeline.status}`;
            console.error(error);
            throw new Error(error);
        }

        console.debug("Pipeline done", [id], pipeline);

        // Pipeline has completed, return latest details
        return pipeline;
    }

    async cancel(pipeline_id: number) {
        const { project_id } = this;
        const action = `/api/v4/projects/${project_id}/pipelines/${pipeline_id}/cancel`;
        await apiFetch(action, { method: "POST" });
        return await this.fetch(pipeline_id);
    }

    async getJobs(pipeline_id: string) {
        const { project_id } = this;
        return await apiFetch(`/api/v4/projects/${project_id}/pipelines/${pipeline_id}/jobs`);
    }
}