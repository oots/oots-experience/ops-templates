import { AppContext } from "../app";
import { getBranch } from "../config";
import { apiFetch } from "../gitlab/api";
import { WorkflowConfig, isPending } from "./config";
import { ProjectContext } from "./projects";
import { PipelineContext } from "./pipelines";
import { JobsContext } from "./jobs";

export class WorkflowContext {
    Projects: ProjectContext;
    Pipelines: PipelineContext;
    Jobs: JobsContext;

    constructor(private config: WorkflowConfig) {
        const { project_id } = this.config;
        if (!project_id) throw new Error('WorkflowContext requires a project_id');

        this.Projects = new ProjectContext(project_id);
        this.Pipelines = new PipelineContext(project_id);
        this.Jobs = new JobsContext(project_id);
    }
    
    isPending(status: string) { return isPending(status) }

    isReady(): boolean {
        // Ensure project exists
        const { project_id } = this.config;
        if (!project_id) return false;
        if (!this.Projects.Cached[project_id]) return false;

        // Make sure the user is logged in
        return !!AppContext.Current?.Auth?.isAuthenticated();
    }

    async getBranch(): Promise<string | null> { return getBranch(); }

    async selectBranch(branch: string) {
        const url = new URL(window.location.href);
        url.searchParams.set("branch", branch);
        if (window.history.pushState) {
            window.history.pushState(null, "", url.toString());
            return branch;
        } else {
            window.location.href = url.toString();
            return new Promise(() => console.log(`Redirecting to: ${url}`));
        }
    }

    async fetchBranches() {
        const { project_id } = this.config;
        const resp = await apiFetch(`/api/v4/projects/${project_id}/repository/branches?per_page=50`);

        // Set default branch (if not set)
        let branch = await this.getBranch();
        if (!branch && resp?.length) {
            branch = resp.filter((i: any) => i.default)[0]?.name;
            console.debug("Branch selected", [branch]);
            if (branch) await this.selectBranch(branch);
        }

        return resp
            .filter((item: any) => item.protected || item.default || !item.merged)
            .sort((a, b) => b?.commit?.created_at?.localeCompare(a?.commit?.created_at))
            .map((item: any) => item.name);
    }
}
