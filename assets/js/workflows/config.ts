export interface WorkflowConfig {
    apiURL: string;
    project_id: string;
    branch?: string;
    environments?: Record<string, string>;
}

export function isPending(status: string) {
    const pendingStates = [
        "running",
        "preparing",
        "pending",
        "created",
        "waiting_for_resource",
    ];
    return pendingStates.indexOf(status) >= 0;
}