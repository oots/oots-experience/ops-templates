import { getBranch } from "../config";
import { apiFetch } from "../gitlab/api";
import { isPending } from "./config";

export class JobsContext {
    constructor(protected project_id: string) { }

    async waitFor(job: any) {        
        const id = job.id;
        console.debug("Wait for job", [id], job);

        // Wait for the pipeline to complete
        while (isPending(job.status)) {
            await new Promise((res) => setTimeout(res, 1000)); // Wait a second
            job = await this.get(id);
        }

        // Handle error conditions for wwhen a pipeline does not complete
        if (["error", "canceled"].indexOf(job.status) >= 0) {
            console.debug(" - job", [id], job.status);
            throw new Error(`Pipeline '${id}' failed, status: ${job.status}`);
        }

        console.debug("Job done", [id], job);

        // Pipeline has completed, return latest details
        return job;
    }

    async getLatest(
        job_name: string,
        {
            branch = getBranch(),
            states = ["success"],
            wait = false,
        } = {}
    ) {
        const { project_id } = this;
        const res = await apiFetch(`/api/v4/projects/${project_id}/jobs/`);

        // Find a recent job that is not in a failed state
        branch = branch || getBranch()
        let job = res?.find((job: any) => {
            if (job.name != job_name) return false;
            if (job.ref != branch) return false;
            if (states.indexOf(job.status) >= 0) return true;
            if (wait && isPending(job.status)) return true;
            return false;
        });

        // Check if we should wait for the job to finish
        if (wait && isPending(job?.status)) {
            job = await this.waitFor(job);
            if (job?.status != "success") {
                // Last job we waited on failed, try and find another recent one instead
                return this.getLatest(job_name, { branch, states, wait });
            }
        }

        return job;
    }

    async get(job_id: string) {
        const { project_id } = this;
        return await apiFetch(`/api/v4/projects/${project_id}/jobs/${job_id}`);
    }

    async getArtifact(job_id: number, path: string) {
        const { project_id } = this;
        return await apiFetch(`/api/v4/projects/${project_id}/jobs/${job_id}/artifacts/${path}`);
    }

    async list() {
        const { project_id } = this;
        return await apiFetch(`/api/v4/projects/${project_id}/jobs/`);
    }
}
