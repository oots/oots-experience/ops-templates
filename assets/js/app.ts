import { AuthContext } from "./auth";
import { AuthTokens } from "./auth/tokens";
import { AppConfig } from "./config";
import { WorkflowContext } from "./workflows/context";

export class AppContext {
    Config: AppConfig;
    Auth?: AuthContext;
    Tokens?: AuthTokens;
    Workflow?: WorkflowContext;

    static get Current() { return window['App'] as AppContext }

    constructor(private params: any) {
        const { auth, workflows } = params;

        // Set up global app config
        this.Config = new AppConfig(params);

        // Load authentication providers if defined
        if (auth) {
            this.Auth = new AuthContext(auth);
        }

        // Optionally load if valid project ID given
        if (workflows) {
            this.Workflow = new WorkflowContext(workflows);
            this.Tokens = new AuthTokens(workflows.project_id);
        }
    }
}