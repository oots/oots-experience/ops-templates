# -----------------------------------------------------------------------------
# Copyright 2024 European Union
#
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
#     https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.
# -----------------------------------------------------------------------------
title: Example Workflows
baseURL: /
publishDir: ../public

params:
  # You can customize your branding with the following settings
  favicon: images/favicon.png
  logo: images/ec-logo.png
  logo_darkmode: images/ec-logo.png
  logo_width: 194px
  logo_height: 32px
  logo_text: Workflows

  # Setup GitLab project for triggering pipelines
  workflow:
    apiURL: https://code.europa.eu
    project_id: "755" # <-- Project ID of your GitLab repository
    environments: # <-- Declare well known branches
      main: Production
      develop: Development

  # Setup the GitLab OAuth2 workflow
  auth:
    type: oauth2
    tokens:
      enabled: true
      allowed: [PRIVATE-TOKEN, DEPLOY-TOKEN]
    oauth2:
      enabled: true
      scope: api openid profile
      client_id: 5a86119b592163b7418221828d71d2b326c60bf067144e36fb81b64d5c515830
      auth_url: https://code.europa.eu/oauth/authorize
      token_url: https://code.europa.eu/oauth/token
      callback_url: https://oots.pages.code.europa.eu/oots-experience/gitlab-pages-templates/user/login/

  # Enable search and indexing of certain sections of this web page
  search:
    enable: true
    primary_color: "#121212"
    include_sections:
      - about
      - workflows
    show_image: false
    show_description: true
    show_tags: true
    show_categories: true

  # Update the copyright info for your web pages by ovverriding this
  copyright: Developed & maintained by the European Commission [OOTS Support Team](https://ec.europa.eu/digital-building-blocks/sites/display/OOTS/Service+Desk), theme credits to [Zen Studio](https://zeon.studio/)
