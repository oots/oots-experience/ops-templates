---
meta_title: About
title: About this page
description: Additional info about this website and the workflows it provides
type: full-width
draft: false
menu:
  main:
    name: About
    weight: 1
  footer:
    name: About
    weight: 1
  sidenav:
    name: About Page Templates
    weight: 1
---

# About this website

This website was generated using the [OOTS Gitlab Pages Templates](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates).

### Getting started

If you want to add or change content on this page, please refer to the [Getting Started](../getting-started/) guide.

### Features included

This website includes the following features:

 - Built-in Gitlab Authentication & Deployment
 - Write and update content in Markdown
 - Indexed Search Functionality
 - Dark Mode & Minimalistic Styling
 - Fully Responsive & Mobile Friendly
 - Page Tags & Categories
 - Syntax Highlighting
 - Pre-designed Pages
    - Homepage
    - Workflows
    - Search
    - About
    - Custom 404
    