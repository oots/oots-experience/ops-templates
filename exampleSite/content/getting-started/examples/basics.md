---
title: Basic workflow example
summary: Oversimplified example using raw HTML as form controls
type: full-width
draft: false
categories:
  - sample
tags:
  - sample
menu:
  sidenav:
    name: Basic Form Usage
    parent: Quick References
    weight: 2
---

{{< toc >}}

{{< form class="workflow p-4 border border-r" >}}
<div>
  <input name="TASK_TO_RUN" value="sample:form:submit" type="hidden">
  <label for="DNS_TARGET">Lookup DNS for:</label>
  <select name="DNS_TARGET">
    <option>code.europa.eu</option>
    <option>www.google.com</option>
    <option>maps.google.com</option>
    <option>mail.google.com</option>
  </select>
  <button type="submit" class="border p-2">Run DNS Lookup</button>
</div>
{{< /form >}}

## About this example 

This is the most basic usage of the [form controls](../../configuration/form-control), that we use to interact with a backend server (GitLab) from a static website.

This example demonstrates the simplest use case with raw HTML templates (eg: _form inputs and buttons_) to trigger a pipeline.

{{< notice "info" >}}

It's generally **_not reccommended_** to use raw HTML templates, as we prefer to use managed [**JSON Forms**](../json-forms) schemas approach. 

{{< /notice >}}

```html
{{< literal text=`{{< form >}}` />}}
<div>
  <input name="TASK_TO_RUN" value="probe:dns" type="hidden">
  <label for="DNS_TARGET">Lookup DNS for:</label>
  <select name="DNS_TARGET">
    <option>code.europa.eu</option>
    <option>www.google.com</option>
    <option>maps.google.com</option>
    <option>mail.google.com</option>
  </select>
  <button type="submit" class="border p-2">Run DNS Lookup</button>
</div>
{{< literal text=`{{< /form >}}` />}}
```

