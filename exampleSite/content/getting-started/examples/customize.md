---
title: Custom Layouts
summary: Using tasks to build more complex workflows
type: full-width
draft: false
categories:
  - sample
tags:
  - sample
menu:
  sidenav:
    name: Custom Layouts
    parent: Quick References
    weight: 5
forms:
  custom-layout-example:
    extends: probe-dns
    partial: probes/dns/info
    scripts: [js/dns/controller.ts]
---

{{< toc >}}

{{< form id="custom-layout-example" debug="true" />}}

# Customizing form layout, styles and scripts

For more advanced use cases, _where auto generated (JSON) forms are not enough_, you can opt to use custom form templates.

Customization is supported by some properties in the front matter:

```yaml
---
# Front matter
forms:
  custom-layout-example:
    class: p-4 border border-red-500    # <-- We can use tailwind css classes here
    partial: probes/dns/index.html      # <-- ./pages/layouts/partial/databases/index.html
    scripts: [js/dns/controller.ts]     # <-- ./pages/assets/js/database.ts
---
```

This example from above will do the following:

1) Apply some custom `css` class names on the form. In our case, we use [`Tailwind CSS`](https://tailwindcss.com/) that is already included.
2) Instead of auto generating forms with [`JSON Forms`](https://jsonforms.io/), we use a custom HTML layout template to generate the view.
3) We also load a custom script, that we use for some more advanced interaction and functionality.

### HTML layout templates

Partial layout templates are located in `./pages/layouts/partials/`, and would typically look something like this:

```html
<div
  class="m-4 border border-gray-500/10"
  x-data="{ 
    controller: null,
    results: null,
  }"
  x-init="addEventListener('load', async (event) => {
    controller = new DnsController();
    results = await controller.fetchData();
    console.log('Fetched data...', results);    
  });
  "
>
  <!-- Header -->
  <div class="flex">
    <h3 class="p-4 flex-1 text-xl">DNS Probes</h3>
    <div class="p-4">{{ partial "probes/dns/toolbar" . }}</div>
  </div>

  <!-- List results -->
  <div class="flex" x-show="results?.length">
    {{ partial "probes/dns/list" . }}
  </div>

  <!-- Loading indicator -->
  <div class="flex" x-show="controller?.loading && results === null">
    <div class="text-xl text-gray-500 opacity-25 py-8 mx-auto">
      <i class="fa-solid fa-arrows-rotate animate-spin"></i>
      <em>Refreshing list of domains...</em>
    </div>
  </div>
</div>
```

You might have noticed some properties like `x-data`, `x-init` and `x-show`. These attributes are used by [`AlpineJS`](https://alpinejs.dev/). AlpineJS is great for adding dynamic interactivity on your page, using simple attribute hooks.

CSS class names come from [`Tailwind CSS`](https://tailwindcss.com/), and is automatically included with the base templates.

Partials can also include additional partials, to break a large view into more manageable pieces, as we have done above.

## Load and use custom scripts

When we require some more advanced functionality, we usually handle this with custom `Javascript` or `Typescript`. 

Scripts are typically located in the `./pages/assets/` folder. Below is an example of a controller written in `Typescript` that is aware of the form `context` object.

```javascript
// File: ./pages/assets/js/dns/controller.ts
import { AppContext } from 'js/app';
import { WorkflowContext } from 'js/workflows/context';

export class DnsController {
    context?: WorkflowContext;

    constructor() {
        this.context = AppContext.Current.Workflow;
    }

    async fetchData(refresh = false) {
      // Returns fetched data...
      const domain = 'example.com';
      return [
          { DNS_TARGET: domain, DNS_TEST_LB: true, PROBE_LIMIT: 10 },
          { DNS_TARGET: "dev." + domain, DNS_TEST_LB: false },
          { DNS_TARGET: "test." + domain, DNS_TEST_LB: false },
      ];
    }

    async submitChanges(item: any) {
      // Spawn the pipeline for the selected item
      const { context } = this;
      const pipeline = await context.Pipelines.start(item);
    }
}

// Expose global objects used by the frontend
declare global {
    interface Window { DnsController: typeof DnsController }
}
window.DnsController = DnsController;
```

Notice how we add the controller class to the window object. This is needed to expose the class to the frontend, as the script is loaded in a sandboxed scope. By adding the class to the window, it will be globally available.

