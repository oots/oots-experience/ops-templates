---
title: Dynamic form generation
summary: Examples of how to auto generate forms using JSON forms
type: full-width
draft: false
categories:
  - sample
tags:
  - sample
menu:
  sidenav:
    name: Form Generators
    parent: Quick References
    weight: 3
forms:
  probe-dns:
    task: probe:dns # <-- Defines the TASK_TO_RUN pipeline ENV var
  only-data: 
    data: &sample-data
      DNS_TARGET: code.europa.eu
      DNS_SERVER: '8.8.8.8'
      DNS_TEST_LB: false
      PROBE_LIMIT: 10
  only-schema:
    data: *sample-data
    schema: &sample-schema
      type: object
      title: Variables
      required:
        - DNS_TARGET
      properties:
        DNS_TARGET:
          title: Target Domain
          type: string
        DNS_SERVER:
          title: DNS Server IP
          type: string
          default: 8.8.8.8
          enum:
            - 1.1.1.1
            - 8.8.8.8
        DNS_TEST_LB:
          title: Test Load Balancers
          type: boolean
          default: false
        PROBE_LIMIT:
          title: Probe Count
          type: integer
          default: 10
          maximum: 50
---

{{< toc >}}

{{< form name="probe-dns" task="sample:form:submit" debug="true" />}}

# About schema-driven forms

We can auto generate forms to _capture input variables_, that will be passed on the the CI server, using [**JSON Forms**](https://jsonforms.io/). Typically we declare:

 - `data` - This is the `variables` that we would like to subbit with a new CI pipeline.
 - `schema` - Declares a data model for the variables thats we would like to display and capture.
 - `uischema` - Optionally we can also declare a better form layout with input validations.

This simplifies and streamlines creating new workflows, and adds rich and interactive form UI's.


## From Usage

The page source and schema files for the form above might look something like this:

{{< tabs markdown="true" >}}
  {{< tab "Page Source" >}}
```yaml
# File: ./pages/workflows/probe-dns.md
---
title: DNS Probes
categories: [support]
tags: [dns]
draft: false
forms:
  probe-dns:
    task: probe:dns # <-- Defines the TASK_TO_RUN pipeline ENV var
---

{{< literal text=`{{< form name="probe-dns" />}}` />}}

```
  {{< /tab >}}
  {{< tab "Initial Data" >}}
```yaml
# File: ./pages/data/forms/probe-dns/data.yaml
DNS_TARGET: code.europa.eu
DNS_SERVER: 8.8.8.8
DNS_TEST_LB: false
PROBE_LIMIT: 10
```  
  {{< /tab >}}
  {{< tab "Data Schema" >}}
```yaml
# File: ./pages/data/forms/probe-dns/schema.yaml
type: object
title: Variables
required:
  - DNS_TARGET
properties:
  DNS_TARGET:
    title: Target Domain
    type: string
  DNS_SERVER:
    title: DNS Server IP
    type: string
    default: 8.8.8.8
    enum:
      - 1.1.1.1
      - 8.8.8.8
  DNS_TEST_LB:
    title: Test Load Balancers
    type: boolean
    default: false
  PROBE_LIMIT:
    title: Probe Count
    type: integer
    default: 10
    maximum: 50
```  
  {{< /tab >}}
  {{< tab "UI Schema" >}}
```yaml
# File: ./pages/data/forms/probe-dns/uischema.yaml
type: VerticalLayout
elements:
  - type: Group
    elements:
      - type: HorizontalLayout
        elements:
          - type: Control
            scope: '#/properties/DNS_TARGET'
            options:
              multi: true
          - type: Control
            scope: '#/properties/DNS_SERVER'
      - type: Control
        scope: '#/properties/DNS_TEST_LB'
        label: Check Load Balancer Info
        options:
          toggle: true
    label: DNS Target Environment
  - type: Group
    label: Test Load Balancers
    elements:
      - type: Control
        scope: '#/properties/PROBE_LIMIT'
    rule:
      effect: SHOW
      condition:
        scope: '#/properties/DNS_TEST_LB'
        schema:
          const: true
```
  {{< /tab >}}
{{< /tabs >}}

The {{< literal text=`{{< form ... />}}` />}} part refers to a [Hugo short code](https://gohugo.io/content-management/shortcodes/), that encapsulates the code required to invoke the pipelines. In our case, this will _bootstrap and generate a form_ (with validation), that can control Gitlab `jobs` and `pipelines`. 

One great feature included in the [JSON Forms Generator](https://jsonforms.io/examples/gen-uischema) library, is the ability to auto generate a default UI layout if none is provided.

This is even possible when only form `data` is provided (_eg: no `schema` or `uischema`_), but this is not reccommended.
{{< notice "note" >}}

Use `schema` definitions to help you define constraints, validation rules and default values.

{{< /notice >}}

{{< tabs markdown="false" >}}  
  {{< tab "With a schema" >}}
    {{< form name="only-schema" task="sample:form:submit" debug="true" />}}
  {{< /tab >}}
  {{< tab "Initial data only" >}}
    {{< form name="only-data" task="sample:form:submit" debug="true" />}}
  {{< /tab >}}
{{< /tabs >}}


## Configuration

We can configure forms in one of a few ways, depending on the use case:

 - [**Page `forms`**](#configure-using-front-mattter) front matter params gives us a way to declare forms as metadata in content.
 - [**Data files**](#configure-using-data-files) enable us to load data, schema and layout from files in the `data/forms/` path.
 
### Configure using 'front mattter'

We can pass parameters to a form using [front matter](https://gohugo.io/content-management/front-matter/). This approach works well _as long as the schema files are not too large_.

```yaml
---
title: JSON Forms Example
forms:
  my-form:
    data: ...     # <-- Declare the initial form values.
    schema: ...   # <-- Define the schema for the varile to capture
    uischema: ... # <-- Optionally declare a custom UI layout 
---

{{< literal text=`{{< form name="my-form" />}}` />}}
```

For more complex forms, we suggest moving schemas and data to files.

### Configure using data files

It's also possible to add `data`, `schema` and `uischema` definitions from a well known `data/forms/` folder path. The sub folder's name coresponding to the name of your form.

This approach of using sepate data files works well, and is the best way to manage more complex workflows.

```text
└── data
    └── forms
        └── my-form
            ├── data.json               // <- Initial form data to load
            ├── schema.json             // <- Data schema model for pipeline variables
            └── uischema.json           // <- UI layout and validation
```

Another advantage of moving to _schema files_ is that it makes it easier to use with [online form builders](https://jsonforms-editor.netlify.app/).
