---
title: Static page example
summary: Simple page with static content
type: full-width
draft: false
categories:
  - sample
tags:
  - sample
menu:
  sidenav:
    name: Static Pages
    parent: Quick References
    weight: 1
---

## Static pages

Static pages are great for quickly adding content on new pages to document a process, procedure or playbook.

You can place a new markdown file anywhere in the `./pages/content/` folder or a subfolder to define a new page. It should contain some [front matter](https://gohugo.io/content-management/front-matter/), eg: the "title" and other metadata, before the markdown body starts.

Here is an example page that also registers itself in the main menu:

```yaml
# File: ./pages/content/example.md
---
title: Examples page
draft: false # <-- If 'true', will not publish this page
menu:
  main: # <-- Registers link in main menu
    name: Example
    pageRef: /example
    weight: 4 # <-- Menu item position
---

# Welcome to my example Page

This is where you can put your own custom contents written in markdown format.

```

{{< figure src="../images/layout-default.png" >}}

As soon as you set `draft: false` on your page, and push it to `git`, GitLab CI/CD will do the work and publish it to your repositories "pages" section.
