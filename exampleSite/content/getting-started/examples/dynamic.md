---
title: Dynamic form content
summary: Using tasks to build more complex workflows
type: full-width
draft: false
categories:
  - sample
tags:
  - sample
menu:
  sidenav:
    name: Dynamic Content
    parent: Quick References
    weight: 4
forms:
  dynamic-content-example:
    extends: probe-dns
    task: sample:form:submit
    zload: 
      task: sample:form:init
      data: exampleSite/data/forms/probe-dns/data.json
      schema: exampleSite/data/forms/probe-dns/schema.json
      #uischema: exampleSite/data/forms/probe-dns/uischema.json
---

{{< toc >}}

{{< form id="dynamic-content-example" debug="true" />}}

# About dynamic form content

We support some settings in the `load` parameters, that we can use to triggger a GitLab pipeline `job` (or `task`), that can dynamically load updated metadata for `data`, `schema` and `uischema` definitions. 

```yaml
---
# Front matter
forms:
  dynamic-content-example:
    task: sample:submit
    load: 
      task: sample:load
      data: temp/generated/data.json
      schema: temp/generated/schema.json
      uischema: temp/generated/uischema.json
---
```

When the form initially loads, we check for a `.load.task` property. If this property is specified, we will:

1) First try and _locate a recent_ GitLab `job` (identified by the `task` name). If found, we will use this cached job ID.
2) If no cached job could be found, we will trigger a new pipeline for the job, and wait for the job to complete. 
3) Once we have the completed job's ID, we try and load artifacts _specified by the paths_ for `data`, `schema` and `uischema`.
4) Use the (updated) artifact data from the `job` we ran, as the new form metadata.

The process above can also be used to _refresh form data on demand_, as it follows the same sequence of steps.

