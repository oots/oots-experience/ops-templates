---
title: layouts
type: full-width
draft: false
menu:
  sidenav:
    parent: Folder structure
    weight: 10
---

# Pages / Layouts

Virtually every part of this website is customizable, thanks to the way [Hugo uses templating, partials and layouts](https://gohugo.io/templates/).

There are a few different template types available, the most notable being:

 - [Base templates](#base-templates) - Typically used to build the website's outer shell, by wrapping other templates within.
 - [Partial templates](#partial-templates) - Can be included in other templates. This encourages reusability and breaking templates into components. 
 - [Home template](#home-template) - Your website's default landing page. You can easily customize this for your specific needs.
 - [Shortcodes](#shortcodes) - Provides a way to include templated HTML into your (markdown) content pages. For example, form controls.

## Base Templates

By default, Hugo will check if a base template defined in `./pages/layouts/_default/baseof.html`. If no template is defined there, it will fall back to your [theme's base template](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates/-/blob/main/layouts/_default/baseof.html?ref_type=heads).

Usually, you do not need to override the base template, but rather the partials that is being included in the `baseof.html`. 

For example, if you wanted to customize the footer, we should override `{{ partial "essentials/footer.html" . }}`. You would define a template for your footer at `./pages/layouts/partials/essentials/footer.html`.

```html
<!-- Path: ./pages/layouts/partials/essentials/footer.html -->
<footer class="bg-theme-light dark:bg-darkmode-theme-light">
  <div class="container">
    <div class="row items-center py-10">      
      <!-- TODO: Custom footer here -->
    </div>
  </div>
  <div class="border-border dark:border-darkmode-border border-t py-7">
    <div class="text-light dark:text-darkmode-light container text-center">
      {{ site.Params.copyright | markdownify }}
    </div>
  </div>
</footer>
```

This template will now be used instead of the one provided by the theme. 

For more information, see the hugo documentation for [Base templates](https://gohugo.io/templates/types/#base).

## Partial Templates

The example above (customizing the footer) nicely illistrates how any part of the website is customizable, by just providing a custom layout `partial` in a well known and expected path.


For more information, see the hugo documentation for [Partial templates](https://gohugo.io/templates/types/#partial).

## Home Template

For more information, see the hugo documentation for [Home templates](https://gohugo.io/templates/types/#home).

## Shortcodes

For more information, see the hugo documentation for [Shortcodes](https://gohugo.io/templates/types/#shortcode).
