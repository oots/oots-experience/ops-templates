---
title: content
type: full-width
draft: false
menu:
  sidenav:
    parent: Folder structure
    weight: 10
forms:
  dns-probes:
    task: probe:dns # <-- Defines the TASK_TO_RUN pipeline ENV var
    data: 
      DNS_TARGET: code.europa.eu
      DNS_SERVER: 8.8.8.8
      DNS_TEST_LB: false
      PROBE_LIMIT: 10
---

# Pages / Content

We model our website's pages as a set of `Markdown` files in folders. This is based on [Hugo's content organization techniques](https://gohugo.io/content-management/organization/). 

```text
└── content
    └── about.md                        // <- https://example.org/about/
    ├── workflows
    |   ├── simple.md                   // <- https://example.org/workflows/simple/
    |   ├── devops
    |   |   └── db-operations.md        // <- https://example.org/workflows/devops/db-operations/
    |   ├── support
    |   |   └── cloudwatch-logs.md      // <- https://example.org/workflows/support/cloudwatch-logs/
    |   |   └── dns-probes.md           // <- https://example.org/workflows/support/dns-probes/
    └── posts
        ├── first.md                    // <- https://example.org/posts/first/
        └── second.md                   // <- https://example.org/posts/second/
```

We can define layout templates for specific sub page types, for example, a `post` might use the default layout, where as `workflows` or `about` pages might use a custom layout template. 

We currently support the following page template types:

 - [**default**](#default-layout) - Optimised for a simple blog-style page layout with a basic heading and content body
 - [**workflow**](#workflow-layout) - Workflow layouts enables us to turn the page into dynamic forms that can interact with the GitLab pipelines.
 - [**custom**](#custom-page-layouts) - _You can also additionally define your own custom page layouts for your use cases_

By default, the layout `type` will be determined by the name of the top-most folder in `./pages/content`. This can also be overwritten by specifying the `type` setting in the page's _front matter_.



## Workflow Layout

For GitLab pipeline automations and workflows, we have a special folder `./pages/content/workflows/`. Workflow pages supports the following additional front matter properties:

 - `categories` - Groups similar workflows logically into the same categories when searching.
 - `tags` - Similar to `categories`, these tags are used to easily switch to workflows of similar nature.
 - `forms` - This is where we can define some metadata related to form generation and what task to run ion the CI pipeline.

For more information, see the [form controls configuration](../../configuration/form-control/) options.

For more information on how to auto generate forms, [see our section on using JSON forms](../../examples/json-forms).

For more information about customizing layouts, [see the custom templates example](../../examples/customize)

## Custom Page Layouts

It's also possible to create your own custom page layouts. For this we can add a new folder `./page/layouts/<YOUR_LAYOUT_TYPE>/` for your custom type. Hugo expects the following files to be defined:

```text
./page/layouts/
└── <YOUR_LAYOUT_TYPE>
    ├── list.html            # Default landing page listing all items
    └── single.html          # This is the template used to render your page    
```

For more information, see the next section on [using layouts and partials](../layouts/) to define custom templating for your forms.
