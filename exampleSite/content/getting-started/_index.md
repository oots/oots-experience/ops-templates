---
title: Getting Started
type: full-width
draft: false
menu:
  main:
    weight: 1
  sidenav:
    weight: 1
---

# Getting started with Gitlab Page Templates

GitLab allows you to publish a static website with your page content, right inside the GitLab server, side-by-side to your code repository. 

This has several advantages, including:

 - No additional infrastructure needed to host your website.
 - Authentication and permissions are aligned to your code repository.
 - Quickly and easily add new 'pages' to your website using simple GitOps.
 - Fully automated CI/CD using GitLab pipeline templates.

This guide will help you define a `pages` section in your repository, using [Hugo](https://gohugo.io/documentation/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/): 

 - [Installation](./installation)
 - [Configuration](./configuration)
 - [Folder structure](./folders/)
 - [Local development](#local-development)
 - [Publish with GitLab](#publishing-your-website-using-gitlab)
 - [Quick Reference Guide](./examples/)



## Local development

At this point we should be all set with a new website. In theory we could directly skip to publishing the website, but it's often best to test and validate changes locally, before handing over control to the CI/CD for deployment.

To run the website `locally`, you can run the following commands:

```bash
# Install dependencies and run in development mode
cd ./pages
npm install
hugo server
# --> Browse to http://localhost:1313/
```

This will run a [local development server in watch mode](https://gohugo.io/commands/hugo_server/), and auto refresh the website contents as you make changes to it. 

Local development mode enables you to _develop and test_ more advanced `workflow` and `layout` templates and how they would render on a website.

Once we are happy with the website's look and feel, the next step is to publish it using gitlab pages.

## Publishing your website using GitLab

We provide a set of [Gitlab pages pipeline templates](../.gitlab/pages.yml), that simplifies the job of publishing your website. No additional cloud infrastructure needs to be provided, as everything is hosted on the GitLab server itself. 

```yaml
# File: .gitlab-ci.yml

include: 
  # Please Note: For remote repositories, you can include templates like so:
   - project: oots/oots-experience/gitlab-pages-templates
     file: .gitlab/pages.yml
```

Once you merge your changes into your default branch, the `pages` is rendered and published to `./public`, and from there to `Gitlab Pages` automatically.

For authentication, we use the same _roles and permissions_ as your source code repository. This simplifies maintenance and ensures that only authorized users can interact with the website/pipelines, as an authenticated user.


