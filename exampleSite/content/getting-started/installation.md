---
title: Installation
type: full-width
draft: false
menu:
  sidenav:
    parent: Getting Started
    weight: 1
---

# Installation Guide

If you do not already have a `./pages` folder in your repository, we need to add a copy of the base templates to your repo. You should have something like this:

```text
pages/                  # <-- This folder contains your website
  assets/               # <-- Define custom images, css & javascript (optional)
  content/              # <-- Where you define the website contents
  data/                 # <-- Metadata used to generate the website (optional)
  hugo.yaml             # <-- Configure and customize the website
  ...
```

If you already have the base templates, you should go and [configure your website](#configuration) for the target repository.

For more information about the folders structure, [see our guide on folder structure here](./folders/).

## Prerequisites

To build the page templates, you need to have some prerequisites installed on your machine.

- [Git](https://git-scm.com/)
- [Go v1.22+](https://go.dev/doc/install)
- [Hugo v0.124+](https://gohugo.io/installation/)
- [Node v20+](https://nodejs.org/en/download/)

These dependencies are not required for the final website, as its all static HTML, CSS and Javascript.

## Seeding your `./pages` folder

Grab a copy of the [example website](https://code.europa.eu/oots/oots-experience/gitlab-pages-templates/-/tree/main/exampleSite?ref_type=heads) folder and copy it to your repositories `./pages` folder:

```bash
git clone git@code.europa.eu:oots/oots-experience/gitlab-pages-templates.git ./temp/pages
mv ./temp/pages/exampleSite ./pages
rm -rf ./temp/pages
```

If you have all the prerequisites installed locally, you can try and build the website using the following commands:

```bash
# working dir: ./pages
npm install # <-- Install dependencies
hugo        # <-- Do a production build to '../public'
```

Remember to commit your changes back to the code repository.

### Next Step: Configure your website

Now that we have the `pages` folder in our repository, proceed to the [configuration section](../configuration/).