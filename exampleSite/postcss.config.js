// Copyright (C) 2024 European Union
// 
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
// European Commission – subsequent versions of the EUPL (the “Licence”);
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// 
// Unless required by applicable law or agreed to in writing, software distributed under
// the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR CONDITIONS
// OF ANY KIND, either express or implied. See the Licence for the specific language
// governing permissions and limitations under the Licence.

const isProd = process.env.HUGO_ENVIRONMENT === "production";
const safelist = [
  /^swiper-/,
  /^lb-/,
  /^gl/,
  /^go/,
  /^gc/,
  /^gs/,
  /^gi/,
  /^gz/,
  /^gprev/,
  /^gnext/,
  /^desc/,
  /^zoom/,
  /^search/,
  /^:is/,
  /dark/,
  /show/,
  /dragging/,
  /fullscreen/,
  /loaded/,
  /visible/,
  /current/,
  /active/,
  /mark/,
];

const purgecss = {
  content: ["./hugo_stats.json"],
  defaultExtractor: (content) => {
    const elements = JSON.parse(content).htmlElements;
    return [
      ...(elements.tags || []),
      ...(elements.classes || []),
      ...(elements.ids || []),
    ];
  },
  safelist,
};

module.exports = {
  plugins: {
    tailwindcss: {},
    autoprefixer: isProd ? {} : false,
  },
};
