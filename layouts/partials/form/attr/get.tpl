{{ $ctx := index . 0 }}
{{ $attr := index . 1 }}
{{ $name := index . 2 | default $attr }}
{{ $value := $ctx.Get $attr }}
{{ if not $value }}
    {{ $ident := partial "form/attr/ident.tpl" $ctx }}
    {{ $node := index $ctx.Page.Params "forms" $ident }}
    {{ range (split $name ".") }}
        {{ $node = index $node . }}
    {{ end }}
    {{ $value = $node }}
{{ end }}
{{ return $value }}