#!/usr/bin/env bash
# -----------------------------------------------------------------------------
set -euo pipefail # Stop running the script on first error...
# -----------------------------------------------------------------------------

config() {
    require dig "Dig utility is required, but not found..."
    colors # Load terminal colors used for pretty printing

    # Check if custom domain was specified
    export DNS_TARGET=${1:-'code.europa.eu'}
    export DNS_TEST_LB=${DNS_TEST_LB:-false}
    export PROBE_LIMIT=${PROBE_LIMIT:-10}

    # Set DNS info and where to store the results
    export DNS_SERVER=${DNS_SERVER:-"8.8.8.8"} # Default DNS server to use
    export DNS_DATA=${DNS_DATA:-"./temp/dns"}    
    export FORM_PATH=${FORM_DATA:-"./exampleSite/data/forms/probe-dns"}    
}

run() {
    config $@ # Parse command line args and set defaults

    # Probe the selected target    
    probe $DNS_TARGET

    # Generate the DNS probe report
    local output="$DNS_DATA/report.json"
    echo "" > "$output"
    while IFS= read -r line; do
      if [ ! -z "${line:-}" ]; then
        file="$line" yq -i -o=json ". += [load(strenv(file))]" "$output" 
      fi
    done < <(find $DNS_DATA/*/info.yaml)

    # Save the new defaults for the form inputs
    yq '.DNS_TARGET = strenv(DNS_TARGET) 
      | .DNS_SERVER = strenv(DNS_SERVER)
      | .DNS_TEST_LB = env(DNS_TEST_LB)
      | .PROBE_LIMIT = env(PROBE_LIMIT)
    ' -i "$FORM_PATH/data.json"
}

probe() {
    local domain=$1

    printf "\n${F_H1}Checking: ${F_HREF}${domain}${L_END}"
    rm -rf "$DNS_DATA/$domain" # Clear prev. results
    mkdir -p "$DNS_DATA/$domain"

    # Track the metadata in a yaml file
    DNS_META="$DNS_DATA/$domain/info.yaml"
    touch "$DNS_META"
    metadata '.domain=strenv(value)' "$domain"

    # Get the authorative server info
    dns_get_authority $domain
    dns_get_nameservers $domain
    dns_get_loadbalancers $domain
}

metadata() {
  local query=${1:-}
  local value=${2:-}
  value=${value:-} yq -i "$query" "$DNS_META"
}

dns_get_authority() {
  local target=$1
  local authority=$(dig @$DNS_SERVER $target SOA +short)
  if [ -z "${authority}" ]; then
    # Empty: Resolve parent DNS authority
    result=$(dig @$DNS_SERVER $target SOA | grep -v ';' | grep SOA)
    parent_id=$(echo "${result:-}" | cut -d $'\t' -f1)
    authority=$(echo "${result:-}" | cut -d $'\t' -f5)
    printf " - Parent Domain: ${C_INFO}${parent_id}${L_END}"
    metadata '.authority.parent=strenv(value)' "$parent_id"
  fi
  if [ ! -z "${authority}" ]; then
    metadata '.authority.target=strenv(value)' "$authority"
    authority="${C_BOLD}${C_INFO}${authority}${C_NONE}"
    printf " - DNS Authority: ${authority}${L_END}"
  fi
}

dns_get_nameservers() {
  # Check for any name servers
  local target=$1
  local nameservers="$DNS_DATA/$target/nameservers.txt"
  dig @$DNS_SERVER $target NS +short > "${nameservers}"
  if grep -q . "${nameservers}"; then
    local count=$(cat ${nameservers} | wc -l | xargs)
    printf " - Name Servers (found ${count})${L_END}"    
    metadata '.nameservers = []' # Clear prev. results
    while IFS= read -r line; do
      if [ ! -z "${line:-}" ]; then
        printf "   - ${C_INFO}${line}${L_END}";
        metadata '.nameservers += [strenv(value)]' "$line";
      fi
    done < <(cat "${nameservers}")
  fi
}

dns_get_loadbalancers() {
    # Check for load balancers
    local target=$1
    local loadbalancers="$DNS_DATA/$target/loadbalancers.txt"
    dig @$DNS_SERVER $target A +short > "${loadbalancers}"

    metadata '.loadbalancers = []' # Clear prev. results

    if ! grep -q . "${loadbalancers}"; then
      printf " - Load Balancers: ${C_DEBUG}none${L_END}"
    else
      local count=$(cat ${loadbalancers} | wc -l | xargs)
      printf " - Load Balancers (found ${count})${L_END}"
      while IFS= read -r line; do
        local host=${line}
        local addr=""

        # Try and resolve the address to a hostname
        if [[ "$host" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
          addr=$host
          host=$(dig -x ${line} +short)
        fi

        if [ ! -z "${line:-}" ]; then 
          printf "   - ${C_INFO}${host} ${C_DEBUG}(${addr})${L_END}";
          metadata '.loadbalancers += [env(value)]' "{ host: \"${host:-}\", addr: \"${addr}\" }";
        fi
      done < <(cat "${loadbalancers}")
    fi
}

colors() {
    C_NONE="\e[0m"
    C_WHITE="\e[97m"
    C_DEBUG="\e[90m"
    C_INFO="\e[94m"
    C_BOLD="\e[1m"
    C_GREEN="\e[32m"
    C_RED="\e[31m"
    C_YELLOW="\e[33m"
    F_H1="\e[4m${C_WHITE}\e[1m"
    F_HREF="\e[4m\e[34m"
    L_END="\n${C_NONE}"
}

require() {
    command -v $1 >/dev/null 2>&1 || { echo >&2 "$2"; exit 1; }
}

throw() {
    printf "${C_RED}$1${L_END}\n" 1>&2
    exit 1
}

# Bootstrap script
run $@
