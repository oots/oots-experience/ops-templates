#!/usr/bin/env bash
# -----------------------------------------------------------------------------
set -euo pipefail # Stop running the script on first error...
# -----------------------------------------------------------------------------
THIS_DIR=$(cd `dirname $0`; pwd)

config() {
    require yq "Tool 'yq' is required, but not found..."

    DNS_TARGET=${DNS_TARGET:-'code.europa.eu'}
    DNS_SERVER=${DNS_SERVER:-"8.8.8.8"} # Default DNS server to use
    FORM_PATH=${FORM_PATH:-"./exampleSite/data/forms/probe-dns"}    
}

run() {
    config $@ # Parse command line args and set defaults

    echo "Generating JSON forms and schemas..."

    # Generate initial state (if not exists)
    if [ ! -f "$FORM_PATH/data.json" ]; then
        cat << EOF > "$FORM_PATH/data.json"
{
  "DNS_TARGET": "${DNS_TARGET}",
  "DNS_SERVER": "${DNS_SERVER}",
  "DNS_TEST_LB": false,
  "PROBE_LIMIT": 10
}
EOF
    fi

    # Add an updated list of DNS_SERVER options (in form schema)
    # get-eu-nameservers \
    # | sed -e 's|^| - "|' -e 's|$|"|' \
    # | yq -i '.properties.DNS_SERVER.enum = load("/dev/stdin")' "$FORM_PATH/schema.json"
}

get-eu-nameservers() {
    curl -s "https://public-dns.info/nameservers.csv" \
    | yq -r -p=csv -o=yaml 'filter(
        .country_code == "AT" or 
        .country_code == "BE" or 
        .country_code == "BG" or 
        .country_code == "HR" or 
        .country_code == "CY" or 
        .country_code == "CZ" or 
        .country_code == "DK" or 
        .country_code == "DE" or 
        .country_code == "EE" or 
        .country_code == "FI" or 
        .country_code == "FR" or 
        .country_code == "GR" or 
        .country_code == "GB" or 
        .country_code == "HU" or 
        .country_code == "IE" or 
        .country_code == "IT" or 
        .country_code == "LV" or 
        .country_code == "LT" or 
        .country_code == "LU" or 
        .country_code == "MT" or 
        .country_code == "NL" or 
        .country_code == "PL" or 
        .country_code == "PT" or 
        .country_code == "RO" or 
        .country_code == "SK" or 
        .country_code == "SI" or 
        .country_code == "ES" or 
        .country_code == "SE" or 
        .country_code == "CH"
    ) | .[] | .ip_address'
}

require() {
    command -v $1 >/dev/null 2>&1 || throw "$2"
}

throw() {
    printf "${C_RED}$1${L_END}\n" 1>&2
    exit 1
}

# Bootstrap script
run $@
